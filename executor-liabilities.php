<?php include 'header.php'; ?>
    <div class="breadcrumbsWrapper row">
    	<div class="container">
                <div class="row">
                    <div class="col-md-12">
                          <ul class="breadcrumbs clearfix">
                                <li><a href="index.php">Home</a></li>
                                <li class="current">Executor Liabilities</li>
                          </ul>
                    </div>
              </div>
        </div>
    </div>
      
      <!--TIER 1-->
        <div class="row">
            <div class="doublepadv clearfix">
                   <div class="col-md-2">
                        <?php include 'side-nav-executor.php'; ?>
                 </div>
                <div class="col-md-8">
                	<div class="doublepadh">
                    <h1>Executor's Liabilities</h1>
                    <h2>What are my liabilities as an Executor or Administrator?</h2>
                    <p>As the <strong>executor of a will</strong> or <strong>administrator</strong> 
                    you have personal and unlimited 
                    liability, which means that if you make a mistake you  could end up footing 
                    the bill for any financial or legal claims that occur as result of your 
                    actions. This takes effect as soon as you undertake the role.</p>
                    <p>Claims may be brought against the executor in relation to the estate for 
                    up to 12 years after the death of the estate owner has been registered.</p>
                    <h2>What actions as an Executor or an Administrator make me financially or legally 
                    liable?</h2>
                    <ul>
                           <li>Neglecting to properly insure the assets of the estate if it suffers a claim</li>
                        <li>Diminishing the estate through imprudent investment and inadequate book keeping</li>
                        <li>Failure to pay the correct taxes on the estate</li>
                        <li>Selling an asset without the agreement of all the executors involved with the 
                        estate</li>
                        <li>Engaging in an action which constitutes a conflict of interest without declaring 
                        or disclosing your interest to all relevant parties</li>
                        <li>Improperly delegating a decision to someone who has no legal authority over 
                        the estate</li>
                        <li>Paying or distributing goods, chattels or assets to the wrong beneficiary 
                        and then failing to recover those assets or monies to the detriment of other 
                        beneficiaries</li>
                        <li>Failing to identify a creditor who subsequently makes a claim after the 
                        estate has been distributed</li>
                        <li>Missing an overseas bank account relating to the estate which subsequently
                        comes to light and results in a tax fine after the estate has been distributed</li>
                    </ul>
                    <p><strong>We offer a simple effective insurance policy</strong> which protects 
                    <strong>executors</strong> and 
                    <strong>administrators</strong> against financial and legal liabilities and claims 
                    relating to 
                    probate.</p>
                  </div>
               </div>
               <div class="col-md-2">
               </div>
           </div>
       </div>
      <!--END TIER 1-->
      
      
<?php include 'footer.php'; ?>
