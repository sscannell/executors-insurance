<div class="stickySide">
 <div id="sticker">
 	<h6>Monthly Archive 2014</h6>
    <ul class="stickerList">
        <li>
            <a href="#">December</a>
            <span class="numberCount">14</span>
        </li>
        <li>
            <a href="#">November</a>
            <span class="numberCount">10</span>
        </li>
        <li>
            <a href="#">October</a>
            <span class="numberCount">09</span>
        </li>
        <li>
            <a href="#">September</a>
            <span class="numberCount">21</span>
        </li>
        <li>
            <a href="#">August</a>
            <span class="numberCount">18</span>
        </li>
    </ul>
 </div>
</div>
