<?php include 'header.php'; ?>
    <div class="breadcrumbsWrapper row">
    	<div class="container">
                <div class="row">
                    <div class="col-md-12">
                          <ul class="breadcrumbs clearfix">
                                <li><a href="index.php">Home</a></li>
                                <li class="current">Obtain a Quote</li>
                          </ul>
                    </div>
              </div>
        </div>
    </div>
      <!--TIER 1-->
        <div class="row">
            <div class="doublepadv clearfix">
                   <div class="col-md-2">
						<?php include 'side-nav-quote.php'; ?>
                   </div>
                <div class="col-md-8">
                	<div class="doublepadh">
                    <h1>Obtain a Quote Online</h1>
                    <h2>Prerequisites</h2>
                    <p>Before filling out the Executors Insurance quotation form:</p>
                    <ul class="tickList">
                            <li>you must read and understand your <strong>role as an Executor</strong></li>
                        <li>you must have a valid email address so we can send your policy 
                        documentation to you. If you do not currently have an email address 
                        please contact us on 0207 993 6745 and we will be happy to give 
                        you a quote over the phone</li>
                    </ul>
                    <div class="alignCenter">
                        <a href="#" class="btn btn-lg btn-red">Continue &raquo;</a>
                    </div>
                 </div>
               </div>
               <div class="col-md-2">
               </div>
           </div>
       </div>
      <!--END TIER 1-->
      
      
<?php include 'footer.php'; ?>
