<?php include 'header.php'; ?>
    <div class="breadcrumbsWrapper row">
    	<div class="container">
                <div class="row">
                    <div class="col-md-12">
                          <ul class="breadcrumbs clearfix">
                                <li><a href="index.php">Home</a></li>
                                <li class="current">Access My Quote</li>
                          </ul>
                    </div>
              </div>
        </div>
    </div>
      
      <!--TIER 1-->
        <div class="row">
            <div class="doublepadv clearfix">
                   <div class="col-md-2">
						<?php include 'side-nav-quote.php'; ?>
                   </div>
                <div class="col-md-8">
                	<div class="doublepadh">
                    <h1>Access Your Quote</h1>
                    <p>Simply enter your reference number and the postcode of the executor 
                    below and click "Access My Quote"</p>
                    <form class="form-inline" role="form">
                      <div class="form-group">
                        <label for="exampleInputEmail2">Reference</label>
                        <input type="email" class="form-control" id="exampleInputEmail2" placeholder="Enter email">
                      </div>
                      <div class="form-group stdmarl">
                        <label for="exampleInputPassword2">My Postcode</label>
                        <input type="password" class="form-control" id="exampleInputPassword2" placeholder="Password">
                      </div>
                      <br /><br />
                        <button type="submit" class="btn btn-lg btn-red stdmart">Access My Quote</button>  
                     </form>
                 </div>
               </div>
               <div class="col-md-2">
               </div>
           </div>
       </div>
      <!--END TIER 1-->
      
      
<?php include 'footer.php'; ?>
