      <div id="scrollTopWrap" class="row">
          <div class="col-md-12 alignCenter">
              <a href="#top" class="scrollTop">
              </a>
          </div>
      </div>
      
      <!--FOOTER-->
      <div id="footer" class="row">
      		<div class="container">
            	<div class="doublepad">
                    <div class="row">
                        <div class="col-md-3 col-sm-3 col-xs-12">
                             <h6 class="title">Main Navigation</h6>
                             <ul class="footerList">
                             <li><a href="obtain-quote.php">Obtain a Quote</a></li>
                             <li><a href="faq.php">FAQs</a></li>
                             <li><a href="executor-info.php">Executor Information</a></li>
                             <li><a href="case-studies.php">Case Studies</a></li>
                          </ul>
                      </div>
                      <div class="col-md-3 col-sm-3 col-xs-12">
                             <h6 class="title">&nbsp;</h6>
                             <ul class="footerList">
                             <li><a href="#">Search</a></li>
                             <li><a href="https://durellweblink.co.uk/executorsinsurance">Client Login</a></li>
                             <li><a href="blog.php">Blog</a></li>
                             <li><a href="contact.php">Contact Us</a></li>
                          </ul>
                      </div>
                      <div class="col-md-3 col-sm-3 col-xs-12">
                             <h6 class="title">Executor Information</h6>
                             <ul class="footerList">
                             <li><a href="executor-info.php">Executor Duties</a></li>
                             <li><a href="executor-liabilities.php">Liabilities</a></li>
                             <li><a href="executor-probate.php">Probate</a></li>
                             <li><a href="executor-links.php">Useful Links</a></li>
                          </ul>
                      </div>
                      <div class="col-md-3 col-sm-3 col-xs-12">
                      	<h6 class="gItalic">Insure from &pound;255 p/annum</h6>
                         <input class="form-control input-lg" type="text" placeholder="Enter Your Full Name"> 
                         <div id="social" class="row stdmart">
                         		<div class="col-md-12">
                                	<a href="http://www.facebook.com/execsinsurance" target="_blank"><span class="iconFacebook"></span></a>
                                <a href="https://twitter.com/ExecsInsurance" target="_blank"><span class="iconTwitter"></span></a>
                             </div>
                         </div>        
                      </div>
                  </div>
              </div>
           </div>
           <div class="copyright">
           	<div class="container">
            		<div class="stdpadv doublepadh">
                    <div class="row">
                    	  <div class="col-md-6 col-xs-12">
                        &copy; CastleAcre Insurance Ltd, 2013, Authorised and Regulated by the 
                        <a href="http://www.fca.org.uk/" target="_blank">Financial Conduct Authority.</a><br />
                        <a class="copyrightLink" href="index.php">Home</a>
                        <a class="copyrightLink" href="#">Sitemap</a>
                        <a class="copyrightLink" href="#">Administration</a>
                        </div>
                        <div id="bottomLogos" class="col-md-6 col-xs-12 alignRight">
                        		<img src="images/footer-logos.jpg" />
                        </div>
                	</div>
                </div>  
             </div>      
           </div>
      </div>
      <!--END FOOTER-->
    
    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://code.jquery.com/jquery.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="js/jquery.sticky.js"></script>
	  <script>
        $(document).ready(function(){
          $("#sticker").sticky({ topSpacing: 20 });
        });
      </script>
    <script src="js/bootstrap.js"></script>
	<script>
    $("a[href='#top']").click(function() {
      $("html, body").animate({ scrollTop: 1 }, "slow");
      return false;
    });
    </script>
    
    
  </body>
</html>