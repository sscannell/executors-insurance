<?php include 'header.php'; ?>
    <div class="breadcrumbsWrapper row">
    	<div class="container">
                <div class="row">
                    <div class="col-md-12">
                          <ul class="breadcrumbs clearfix">
                                <li><a href="index.php">Home</a></li>
                                <li class="current">Contact</li>
                          </ul>
                    </div>
              </div>
        </div>
    </div>
      
      <!--TIER 1-->
      <div class="row">
          <div class="container">
                <div class="row">
                    <div class="doublepad clearfix">
                    	   <div class="col-md-1">
                         </div>
                        <div class="col-md-10">
                            <h1>Contact Us</h1>
                            <div class="row">
                            		<div class="col-md-6">
                                    <h2>General Enquiries</h2>
                                    <p class="big">0207 993 6745</p>
                                    <h2>Claims Hotline</h2>
                                    <p class="big">0207 280 9000</p>
                                		<h2>Email</h2>
                                    <a class="big" href="mailto:enquiries@executorsinsurance.co.uk">
                                    enquiries@executorsinsurance.co.uk</a>
                                </div>
                                <div class="col-md-6">
                                		<h2>Address</h2>
                                    <p class="big">Executors Insurance<br />
                                    c/o Castleacre Insurance Services Ltd<br />
                                    Cygnet Court<br />
                                    Swan Street<br />
                                    Boxford<br />
                                    Suffolk<br />
                                    CO10 5NZ
                                </div>
                            </div>
                       </div>
                       <div class="col-md-1">
                       </div>
                   </div>
               </div>
          </div>
      </div>
      <!--END TIER 1-->
      
      
<?php include 'footer.php'; ?>
