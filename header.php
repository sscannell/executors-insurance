<!DOCTYPE html>
<html>
  <head>
    <title>Executors Insurance</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="icon" type="image/x-icon" href="favicon.ico" />    
    <link href='http://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800' rel='stylesheet' type='text/css'>
    <link href="css/style.css" rel="stylesheet">
    <!-- Bootstrap -->
    <link href="css/bootstrap.css" rel="stylesheet">    

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->
  </head>
  <body>
  	
    <div class="row borderBottom">
        <div class="container">
            <!--MAIN NAVIGATION -->
            <nav class="navbar navbar-default" role="navigation">
              <!-- Brand and toggle get grouped for better mobile display -->
              <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                  <span class="sr-only">Toggle navigation</span>
                  <span class="icon-bar"></span>
                  <span class="icon-bar"></span>
                  <span class="icon-bar"></span>
                </button>

                <a class="navbar-brand" href="index.php"><img src="images/logo.svg" width="170" /></a>
              </div>
            
              <!-- Collect the nav links, forms, and other content for toggling -->
              <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
              <?php $page = $_SERVER['SCRIPT_NAME']; ?>
                <ul class="nav navbar-nav">
                   <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">Online Quote 
                    <b class="caret"></b></a>
                    <ul class="dropdown-menu">
                      <li><a href="obtain-quote.php">Obtain a Quote</a></li>
                      <li class="divider"></li>
                      <li><a href="access-quote.php">Access My Quote</a></li>
                      <li class="divider"></li>
                      <li><a href="access-renewal.php">Access My Renewal</a></li>
                    </ul>
     				 </li>
                  <li <?php if ($page == "/faq.php"){ echo "class='active'";} ?>><a href="faq.php">FAQs</a></li>
                   <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">Executor Information 
                    <b class="caret"></b></a>
                    <ul class="dropdown-menu">
                      <li><a href="executor-info.php">Executor Details</a></li>
                      <li class="divider"></li>
                      <li><a href="executor-duties.php">Executor Duties</a></li>
                      <li class="divider"></li>
                      <li><a href="executor-liabilities.php">Liabilities</a></li>
                      <li class="divider"></li>
                      <li><a href="executor-probate.php">Probate</a></li>
                      <li class="divider"></li>
                      <li><a href="executor-links.php">Useful Links</a></li>
                      <li class="divider"></li>
                      <li><a href="#">Document Library</a></li>
                    </ul>
     				 </li>
                  <li <?php if ($page == "/case-studies.php"){ echo "class='active'";} ?>>
                  <a href="case-studies.php">Case Studies</a></li>
                </ul>
                <ul class="nav-icons">
                  <li><a href="#"><span class="iconSearch"></span><p>Search</p></a></li>
                  <li><a href="https://durellweblink.co.uk/executorsinsurance"><span class="iconLogin"></span>
                  <p>Client Login</p></a></li>
                  <li><a href="blog.php"><span class="iconBlog"></span><p>Blog</p></a></li>
                  <li class="last"><a href="contact.php"><span class="iconContact"></span><p>Contact Us</p></a></li>
                </ul>
              </div><!-- /.navbar-collapse -->
            </nav>
            <!--END NAVIGATION--->
        </div>
    </div>
    