<?php include 'header.php'; ?>
    <div class="breadcrumbsWrapper row">
    	<div class="container">
                <div class="row">
                    <div class="col-md-12">
                          <ul class="breadcrumbs clearfix">
                                <li><a href="index.php">Home</a></li>
                                <li class="current">Executor Duties</li>
                          </ul>
                    </div>
              </div>
        </div>
    </div>
      
      <!--TIER 1-->
        <div class="row">
            <div class="doublepadv clearfix">
                   <div class="col-md-2">
                        <?php include 'side-nav-executor.php'; ?>
                 </div>
                <div class="col-md-8">
                	<div class="doublepadh">
                    <h1>Executors' Duties</h1>
                    <h2>What are the duties of an Executor or Administrator of an Estate?</h2>
                    <p>An executor of a will or administrator, whether professional or non-professional, 
                    has a statutory duty of care to carry out the administration of an estate with 
                    care and skill. The duties of an executor are designed to ensure that the 
                    executor (the deceased's personal representative) or estate administrator 
                    (appointed by the Probate Office) acts in the best interests of the beneficiaries 
                    and avoids loss or injury to the estate. If an executor or estate administrator 
                    breaches their duty they can be held financially liable even when the breach 
                    is not deliberate.</p>
                    <h2>Executors' Duties include:</h2>
                    <ul class="tickList">
                            <li>Notify all beneficiaries and relevant individuals of the death of 
                            the estate owner</li>
                        <li>Apply for Grant of Probate</li>
                        <li>Find and collect in all the assets and liabilities of the deceased</li>
                        <li>Reasonable and prudent management of the estate, including protection 
                        of assets and investments during probate</li>
                        <li>Keep a set of accurate accounts relating to the deceased's estate</li>
                        <li>Pay the tax debts of the deceased's estate including Capital Gains Tax, 
                        Inheritance Tax and Income Tax</li>
                        <li>Pay all creditors of the deceased's estate</li>
                        <li>Correctly distribute the estate to the beneficiaries</li>
                    </ul>
                    <p>For futher information about what to do when someone dies and how to manage 
                    the estate you can download a useful pdf leaflet from the Government website, 
                    Direct Gov, 'What to Do After Death' - 
                    <a href="http://www.direct.gov.uk/en/Governmentcitizensandrights/Death/WhattoDoAfterADeath/DG_10029642" target="_blank">Click Here</a>
                 </p>
                 </div>
               </div>
               <div class="col-md-2">
               </div>
           </div>
       </div>
      <!--END TIER 1-->
      
      
<?php include 'footer.php'; ?>
