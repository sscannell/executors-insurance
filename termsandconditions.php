<?php include 'header.php'; ?>
    
      
      <!--TIER 1-->
      <div class="row">
          <div class="container">
                <div class="row">
                    <div class="doublepad clearfix">
                    	   <div class="col-md-1">
                         </div>
                        <div class="col-md-10">
                            <h1>Terms &amp; Conditions</h1>
                            <p>Executors Insurance is a subsidiary of Castleacre Insurance 
                            Services Ltd (reg in England no.5114821) (FSA registered number 
                            308705) which is an independent insurance intermediary registered 
                            in the UK and regulated by the Financial Conduct Authority (FCA 
                            - www.fca.org.uk)(1 April 2013). WR Berkley (Europe) Ltd are the 
                            insurers who provide Executors Liability cover.</p>
                            <h2>Terms of Business</h2>
                            <p>We are an independent Insurance Broker acting on behalf 
                            of clients to arrange insurance.</p>
                            <div class="greyBack">
                                <div class="stdpad">
                                    <p>Before sending you a quotation and prior to acting as your 
                                    agent we ask you to confirm that your have read and agreed to 
                                    our Terms and Conditions which form part of our contract with 
                                    you from the inception of your policy to its expiry.</p>
                                </div>
                            </div>
                            <p>The following highlights important insurance practices 
                            and procedures. It also sets out the basis upon which we 
                            will act as your agent. If you do not wish our relationship 
                            to be governed in such a manner, you need to advise us before 
                            we arrange any insurance on your behalf.</p>
                            <ol>
                            		<li>
                                   <h5>The Policy We Are Offering</h5>
                                 	<p>Executors Insurance offers one policy underwritten 
                                    by insurer WR Berkley Europe Ltd.</p>
                                </li>
                            		<li>
                                   <h5>Services We Provide To You</h5>
                                 	<p><strong>We arrange an insurance supplied by WR Berkley 
                                    (Europe)Ltd</strong> - this is a non-advisory online product 
                                    arranged by us to WR Berkley Europe Ltd, we do not 
                                    source the whole market as this product has been 
                                    designed by us with WR Berkley (Europe)Ltd for 
                                    our clients.</p>
                                    <p><strong>Documentation</strong> - Upon acceptance of your request for 
                                    insurance we will send you an email and pdf attachments 
                                    which provide a summary of the main terms of the insurance 
                                    and identifies the insurer (WR Berkley Europe Ltd) with 
                                    whom it has been placed. <strong>Please check this and advise 
                                    us if there is any variance with your instructions 
                                    immediately.</strong></p>
                                    <p>We will also send you details of the premium 
                                    charged by the insurer for the insurance: showing 
                                    the full premium, the relevant tax and our commission.</p>
                                    <p><strong>Premiums</strong> - Premiums must be settled to us by the 
                                    inception of the policy. If any amendments require 
                                    changes to the payment structure these must be 
                                    complied with as soon as instructed.</p>
                                    <p><strong>Amendments</strong> -If you require any amendments to the 
                                    terms of the insurance please contact us as soon as 
                                    possible specifying the required changes with supporting 
                                    documents if necessary. If you need to make amendments 
                                    please contact Executors Insurance on 0207 993 6745 or 
                                    email us at 
                                    <a href="mailto:enquiries@executorsinsurance.co.uk">
                                    enquiries@executorsinsurance.co.uk</a>
                                </li>
                            		<li>
                                   <h5>Duty of Disclosure</h5>
                                 	<p>Insurance contracts are contracts of 'Utmost Good Faith'. 
                                    This means that a proposer of insurance must disclose to the 
                                    insurer(s) all material information relating to the risk 
                                    under consideration. "Material" in this context refers to 
                                    all information which a prudent insurer (not necessarily 
                                    the insurer in question) would wish to take account of 
                                    when considering whether or not to accept the risk and, 
                                    if so, upon what terms and what price.</p>
                                    <p>The <strong>duty of disclosure</strong> continues up until the policy 
                                    is concluded and continues during the period of the policy 
                                    contract. It may also be that the terms of the policy 
                                    include ongoing disclosure conditions or warranties.</p>
                                    <p>In completing the online declaration form or any written 
                                    declaration or in any correspondence regarding claims or any 
                                    material document relating to this insurance policy, the 
                                    accuracy of all answers, statements and/ or information 
                                    is the insured's responsibility.</p>
                                    <p>In the event that there is a breach of the duty of disclosure, 
                                    the insurer may have the right to void the insurance from its 
                                    commencement. Under such circumstance, the insurer would be 
                                    entitled to seek recovery of any claims already paid by 
                                    them under this insurance, although at the same time the 
                                    insurer(s) would generally be obliged to return paid premiums.</p>
                                </li>
                            		<li>
                                   <h5>Cancellation and Refund Policy</h5>
                                 	<p>You have a legal right to cancel your policy for any reason, 
                                    (subject to no claims having occurred) within 14 days of the policy 
                                    inception. You will always be advised where this right applies. If 
                                    you wish to cancel a policy and apply for a full refund you must 
                                    advise us in writing, prior to expiry of the 14 day cancellation 
                                    period to our usual office address:</p>
                                    <p>Executors Insurance, Cygnet Court, Swan Street, Boxford, 
                                    Suffolk CO10 5NZ</p>
                                    <p>Refund payments will be made within 5 working days of receipt 
                                    of your policy cancellation and refund application.</p>
                                    <p>No refund will be made after this 14 day cooling off period.</p> 
                                </li>
                            		<li>
                                   <h5>Claims Procedures</h5>
                                 	<p>Claims should be notified directly to the insurer WR Berkley Europe 
                                    Ltd by calling 0207 280 9000.</p>
                                    <p>If you are in any way unsatisfied with the way in which your claim is 
                                    dealt with, you should contact Executors Insurance Ltd 0207 993 6745 or 
                                    email <a href="mailto:enquiries@executorsinsurance.com">enquiries@executorsinsurance.com</a></p>
                                    <p>We will work closely with you to ensure that you receive as full and 
                                    reasonable a recovery within the terms of the contract in as speedy a 
                                    timeframe as possible.</p>
                                </li>
                            		<li>
                                   <h5>Remuneration</h5>
                                 	<p>Our principle remuneration for arranging your insurance is 25% of the 
                                    base premium excluding Insurance Premium Tax. In addition, you should be 
                                    aware that as a result of arranging the insurance, we may receive 
                                    additional income from the following sources:</p> 
                                    <ul class="tickList">
                                    	<li>Interest earned on monies passing through our insurance broker 
                                        accounts.</li>
                                        <li>In the event that a contract is cancelled after the cooling-off 
                                        period we reserve the right to keep our commission.</li>
                                    </ul>
                                </li>
                            		<li>
                                   <h5>Holding Client Monies</h5>
                                 	<p>We hold monies paid to us on your behalf before paying to insurers. 
                                    All client monies are held in a separate Client Trust Fund account. 
                                    Any interest earned on monies in this account accrues to the benefit 
                                    of Castleacre Insurance Services Ltd.</p> 
                                </li>
                            		<li>
                                   <h5>Privacy Policy</h5>
                                 	<p>Our privacy policy forms part of these terms and condition and can 
                                    be viewed in detail on a separate page <a href="privacy.php">click here 
                                    to view our Privacy Policy in full detail</a></p>
                                </li>
                            		<li>
                                   <h5>Who Regulates Us</h5>
                                 	<p>Executors Insurance, a subsidiary of</p>
                                    <p>Castleacre Insurance Services Ltd<br />Cygnet Court<br />Swan Street
                                    <br />Boxford<br />Suffolk<br />CO10 5NZ</p>
                                    <p>and is authorised and regulated by the Financial Conduct 
                                    Authority (FCA)(1 April 2013). Our registered number is: 308705.</p>
                                    <p>Our permitted business is arranging general insurance contracts.</p>
                                    <p>You can visit the Financial Conduct Authority website at - 
                                    <a href="http://www.fca.org.uk" target="_blank">http://www.fca.org.uk</a></p>
                                    <p>The FCA's Consumer helpline number is - UK: 0800 111 6768 (freephone)</p>
                                </li>
                            		<li>
                                   <h5>Ownership</h5>
                                 	<p>There is no shareholding between any insurer and Castleacre Insurance 
                                    Services Ltd.</p>
                                    <p>Registered in England under the Companies Act 2006 No. 05114821 at 
                                    Cygnet Court, Swan Street, Boxford, Suffolk CO10 5NZ.</p>
                                </li>
                            		<li>
                                   <h5>Complaints</h5>
                                 	<p>If you have a complaint regarding our practices or performance, 
                                    please contact us at the following address:</p>
                                    <p>Executors Insurance<br />c/o Castleacre Insurance Services Ltd<br />
                                    Cygnet Court<br />Swan Street<br />Boxford<br />Suffolk<br />CO10 5NZ</p>
                                    <p>detailing the nature and underlying circumstances of your complaint. 
                                    We will try to resolve this by the close of the next business day. 
                                    If that is not possible we will send you a copy of how we intend 
                                    to handle the situation outlining our complaints handling procedure.</p>
                                    <p>If the complaint is not resolved to your satisfaction you have recourse 
                                    to the Financial Ombudsman Scheme (FOS). The FOS was set up by the 
                                    Financial Services Authority and is independent. It is there to resolve 
                                    disputes between consumers and regulated firms in an efficient manner.</p>
                                    <p>Making a complaint against us is in addition to and does not replace your 
                                    right to seek legal redress against us. Any dispute shall be governed by 
                                    English Law.</p>
                                </li>
                            		<li>
                                   <h5>The Financial Services Compensation Scheme (FSCS)</h5>
                                 	<p>We are covered by the FSCS. You may be entitled to compensation from 
                                    the scheme if we cannot meet our obligations. This depends on the 
                                    type of business and the circumstances of the claim.</p>
                                    <p>Insurance advising and arranging is covered for 100% of the first 
                                    &pound;2,000 and 90% of the remainder of the claim, without any upper limit.</p>
                                    <p>Further information about compensation scheme arrangements is available 
                                    from the FSCS.</p>
                                    <p>TOBA (UK) 1.4.2013</p>
                                </li>
                            </ol>
                       </div>
                       <div class="col-md-1">
                       </div>
                   </div>
               </div>
          </div>
      </div>
      <!--END TIER 1-->
      
      
<?php include 'footer.php'; ?>
