<?php include 'header.php'; ?>
       
      <div id="homepage"> 
    <!--MAIN CAROUSEL-->
        <div id="homepageSlider" class="carousel slide" data-ride="carousel">
          <!-- Indicators 
          <ol class="carousel-indicators">
            <li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
            <li data-target="#carousel-example-generic" data-slide-to="1"></li>
            <li data-target="#carousel-example-generic" data-slide-to="2"></li>
          </ol>-->
        
          <!-- Wrapper for slides -->
          <div class="carousel-inner">
            <div class="item active">
              <img src="images/banner-background-01.jpg" />
              <div class="redTextOverlay">
              		<div class="doublepad">
                        <div class="bannerText">
                        <h1>Insurance to Protect<br />
                        Executors of a Will</h1>
                        <p>When a family member or close friend asks you to act as the executor of their
                        will, most people willingly accept the role as a privileged responsibility but
                        few realise that they potentially face unlimited personal liability with no
                        legal or financial protection.</p>
                        <div class="topLink stdmart">
                            <span class="squareArrow">
                            </span>
                            <a class="bannerLink" href="obtain-quote.php">
                                    Find out how we can help
                            </a>
                        </div>
                      </div>
                  </div>
              </div>
            </div>
            <div class="item mobFull">
              <img src="images/banner-background-01.jpg" />
              <div class="redTextOverlayFull">
              		<div class="doublepad clearfix">
                    <div class="row">
                    	<div class="col-md-6 col-sm-6">
                        <div class="bannerText doublepadh chunkyBorder">
                        <h1>Frequently Asked Questions</h1>
                      </div>
                     </div>
                     <div class="col-md-6 col-sm-6">
                     	   <div class="bannerText stdpadt">
                        <p>Being an executor, or administrator is a serious legal responsibility and 
                        any executor faces the possibility of unlimited personal liability if they 
                        make mistakes in the distribution of an estate. Find out  why you might need 
                        insurance protection as an executor of a will or administrator?</p>
                        <div class="topLink stdmart">
                            <span class="squareArrow">
                            </span>
                            <a class="bannerLink" href="faq.php">
                                    View Other FAQ's
                            </a>
                        </div>
                        </div>
                     </div>
                  </div>
                </div>
              </div>
            </div>
            <div class="item mobFull">
              <img src="images/banner-background-01.jpg" />
              <div class="redTextOverlayFull">
              		<div class="doublepad">
                    <div class="row">
                    	<div class="col-md-6 col-sm-6">
                        <div class="bannerText doublepadh chunkyBorder">
                        <h1>Executor or Administrator Liabilities</h1>
                      </div>
                     </div>
                     <div class="col-md-6 col-sm-6">
                     	   <div class="bannerText stdpadt">
                        <p>As the executor of a will or administrator you have personal and unlimited 
                        liability, which means that if you make a mistake you could end up footing 
                        the bill for any financial or legal claims that occur as result of your 
                        actions. This takes effect as soon as you undertake the role.</p>
                        <div class="topLink stdmart">
                            <span class="squareArrow">
                            </span>
                            <a class="bannerLink" href="executor-liabilities.php">
                                    See More About Liabilities
                            </a>
                        </div>
                        </div>
                     </div>
                  </div>
                </div>
              </div>
            </div>
            <div class="item mobFull">
              <img src="images/banner-background-01.jpg" />
              <div class="redTextOverlayFull">
              		<div class="doublepad">
                    <div class="row">
                    	<div class="col-md-6 col-sm-6">
                        <div class="bannerText doublepadh chunkyBorder">
                        <h1>Don't Get Caught Out With Claims</h1>
                      </div>
                     </div>
                     <div class="col-md-6 col-sm-6">
                     	   <div class="bannerText stdpadt">
                            <p>For most executors the process of probate should be relatively straightforward, 
                            particularly where the estate is small and there is no inheritance tax to contend 
                            with, however there are many recorded instances where lay executors and 
                            administrators have been subject to serious and unexpected claims</p>
                            <div class="topLink stdmart">
                                <span class="squareArrow">
                                </span>
                                <a class="bannerLink" href="case-studies.php">
                                        View Case Studies
                                </a>
                            </div>
                        </div>
                     </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        
          <!-- Controls -->
          <a class="left carousel-control" href="#homepageSlider" data-slide="prev">
            <span class="bannerLeft"></span>
          </a>
          <a class="right carousel-control" href="#homepageSlider" data-slide="next">
            <span class="bannerRight"></span>
          </a>
        </div>
      <!--END CAROUSEL--->
         
          <!--TIER 1-->
          <div class="row alignCenter">
              <div class="container">
                    <div class="row">
                        <div class="doublepad clearfix">
                            <div class="col-md-2">
                           </div>
                            <div class="col-md-8">
                                <h1>Why Does an Executor or Administrator of an Estate Need Insurance
                                Protection?</h1>
                                <p>Even the most straightforward of wills can involve hidden difficulties 
                                and probate regulations are complex - this can leave executors/personal 
                                representatives and administrators vulnerable to potential legal claims 
                                by beneficiaries, estate creditors and the tax office.</p>
                                <p class="red">What Does Our Executors Insurance Policy Offer?</p>
                           </div>
                           <div class="col-md-2">
                           </div>
                       </div>
                   </div>
              </div>
          </div>
            <div class="container">
                    <div class="row">
                        <div class="col-md-12 alignCenter">
                            <img class="negMargin" src="images/grey-arrow.png" />
                      </div>
                   </div>
                    <div class="row borderBottom">
                        <div class="doublepadb">
                            <div class="col-md-12">
                                <div class="greyBox clearfix">
                                        <div class="col-md-4 col-sm-4">
                                            <span class="iconShield"></span><p>Protection against errors made during probate</p>
                                            <br />
                                            <span class="iconMax"></span><p>Flexibility - you choose the level of cover you want</p>
                                        </div>
                                        <div class="col-md-4 col-sm-4">
                                            <span class="iconFile"></span><p>A single annual policy which can be arranged within minutes</p>
                                            <br />
                                            <span class="iconTag"></span><p>Affordable cover - a single premium payment </p>
                                        </div>
                                        <div class="col-md-4 col-sm-4">
                                            <span class="iconLegal"></span><p>Legal Expenses</p>
                                            <br />
                                            <span class="iconPercent"></span><p>Discounted premium for additional executors or extended cover</p>
                                        </div>
                                </div>
                                    <br />
                                    <div class="alignCenter">
                                        <a href="#" class="btn btn-lg">Find Out More</a>
                                        <a href="#" class="btn btn-lg btn-red">Online Quote</a>
                                    </div>
                          </div>
                      </div>
                  </div>
           </div>
          <!--END TIER 1-->
          
          <!--TIER 2-->
          <div id="tier2" class="row">
                <div class="container">
                    <div class="row">
                            <div class="col-md-6 alignCenter borderLeft borderRight greyBack">
                            <div class="doublepad">
                                <h1>Resources for Executors</h1>
                             <ul class="resourcesList">
                                    <li class="iconInfo">
                                    <div class="halfpad">
                                        <h3>Executor Information</h3>
                                        <p>The Trustee Act of 2000 which extends to England
                                        and Wales, has clarified the law on the management
                                        of deceased...</p>
                                    </div>
                                 </li>
                                 <li class="iconFAQ">
                                    <div class="halfpad">
                                        <h3>FAQS</h3>
                                        <p>Being an executor, or administrator is a serious
                                        legal responsibility and any executor faces the
                                        possibility of...</p>
                                    </div>
                                 </li>
                                 <li class="iconBook">
                                    <div class="halfpad">
                                        <h3>How We Can Help</h3>
                                        <p>The cost of taking out Executors Insurance for one
                                        year can be less than the cost of an hour of legal
                                        advice...</p>
                                    </div>
                                 </li>
                             </ul>
                            </div>
                          </div>
                          <div class="col-md-6 alignCenter borderRight">
                            <div class="doublepadt">
                                <h1>Case Studies</h1>
                             </div>
                                    <!--MAIN CAROUSEL-->
                                        <div id="caseStudiesSlider" class="carousel slide" data-ride="carousel">
                                          <!-- Indicators 
                                          <ol class="carousel-indicators">
                                            <li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
                                            <li data-target="#carousel-example-generic" data-slide-to="1"></li>
                                            <li data-target="#carousel-example-generic" data-slide-to="2"></li>
                                          </ol>-->
                                        
                                          <!-- Wrapper for slides -->
                                          <div class="carousel-inner">
                                            <div class="item active">
                                                <div class="casePad">
                                                      <h4>HMRC vs Lever</h4>
                                                    <p class="caseGeorgia">In 2007 Mr Lever, acting as executor for his late
                                                    mother-in-law's estate paid &pound;400,000 in inheritance tax,
                                                    closed the accounts, and distributed the balance of the estate
                                                    to beneficiaries of her will...</p>
                                                    <div class="alignCenter stdpad">
                                                        <a href="case-studies.php" class="btn btn-lg">Find Out More</a>
                                                    </div>
                                               </div>
                                            </div>
                                            <div class="item">
                                                <div class="casePad">
                                                      <h4>Thompson vs Fraser</h4>
                                                    <p class="caseGeorgia">Two sisters made detailed mutual wills 
                                                    (leaving their estates to each other) in 1991. The wills stated 
                                                    that when one sister predeceased the other, further clauses 
                                                    in each other's wills would come into effect...</p>
                                                    <div class="alignCenter stdpad">
                                                        <a href="case-studies.php" class="btn btn-lg">Find Out More</a>
                                                    </div>
                                               </div>
                                            </div>
                                            <div class="item">
                                                <div class="casePad">
                                                      <h4>Shovelar vs Lane</h4>
                                                    <p class="caseGeorgia">Here, in a further case of mutual 
                                                    wills, two widowers with their own separate families married 
                                                    each other and created their mirror wills leaving everything 
                                                    to each other, with the provision that afterwards...</p>
                                                    <div class="alignCenter stdpad">
                                                        <a href="case-studies.php" class="btn btn-lg">Find Out More</a>
                                                    </div>
                                               </div>
                                            </div>
                                            <div class="item">
                                                <div class="casePad">
                                                      <h4>IRC vs Stannard</h4>
                                                    <p class="caseGeorgia">Here, the Inland Revenue attempted to 
                                                    sue the executor of a deceased's estate for approx.&pound;75,000 
                                                    capital transfer tax and interest. The defendant claimed that 
                                                    firstly, as executor he was only a representative...</p>
                                                    <div class="alignCenter stdpad">
                                                        <a href="case-studies.php" class="btn btn-lg">Find Out More</a>
                                                    </div>
                                               </div>
                                            </div>
                                          </div>
                                        
                                          <!-- Controls -->
                                          <a class="left carousel-control" href="#caseStudiesSlider" data-slide="prev">
                                            <span class="bannerLeft"></span>
                                          </a>
                                          <a class="right carousel-control" href="#caseStudiesSlider" data-slide="next">
                                            <span class="bannerRight"></span>
                                          </a>
                                        </div>
                                      <!--END CAROUSEL--->
                          </div>
                  </div>
               </div>
          </div>
          <!--END TIER 2-->
      </div>
      
<?php include 'footer.php'; ?>
