<?php include 'header.php'; ?>
    <div class="breadcrumbsWrapper row">
    	<div class="container">
                <div class="row">
                    <div class="col-md-12">
                          <ul class="breadcrumbs clearfix">
                                <li><a href="index.php">Home</a></li>
                                <li class="current">Probate</li>
                          </ul>
                    </div>
              </div>
        </div>
    </div>
      
      <!--TIER 1-->
        <div class="row">
            <div class="doublepadv clearfix">
                   <div class="col-md-2">
                        <?php include 'side-nav-executor.php'; ?>
                 </div>
                <div class="col-md-8">
                     <div class="doublepadh">
                    <h1>Probate</h1>
                    <h2>Probate Guidelines for Executors and Estate Administrators</h2>
                    <p>We have set out some guidelines for Executors and Estate Administrators. 
                    It is always advisable to seek advice from reputable sources - Government 
                    guidelines on probate and inheritance tax are a good starting place; you 
                    can do this by:</p>
                    <ol>
                            <li>Looking online at <a href="http://www.direct.gov.uk">www.direct.gov.uk</a></li>
                        <li>Visiting your local Probate Office</li>
                        <li>Calling the Probate and Inheritance Tax Helpline, on <strong>
                        0845 302 0900</strong></li>
                    </ol>
                    <p>If you are seeking professional advice it is a good idea to go to regulated 
                    professionals - STEP (the Society of Trust and Estate Practitioners) can 
                    provide further information on the professional management of Probate but 
                    remember that a professional executor will charge a fee - ask for a quotation 
                    rather than an estimate of costs (as a general guide charges normally stand 
                    at around 1.5%-2% of the total estate value) - you may need to consult with 
                    beneficiaries as these fees are normally taken from the estate.</p>
                    <h2>Probate Guidelines</h2>
                    <ul class="tickList">
                            <li><strong>Know what you're getting yourself into</strong><br />being an executor is a 
                            serious legal responsibility, so if a friend or family member asks 
                            you to take on this role take the time to find out exactly what it 
                            involves. There is help available and the online government guidelines 
                            are a good place to start - or if you know a friendly solicitor you can 
                            ask their advice.  It normally takes between six months and a year 
                            to administer an estate so it requires quite a time commitment.</li>
                            <li><strong>Understand what your main tasks are</strong><br />The primary functions of the executor 
                            are to value the estate's assets and debts, pay Inheritance Tax (if applicable) 
                            and creditors, and then distribute the remaining assets to beneficiaries. It 
                            is essential to keep accurate, transparent records of everything you do as 
                            an executor; this is especially important if Inheritance Tax is payable 
                            (currently at 40% on estates over &pound;325,000 (2012 -2013)). While most estates 
                            are relatively straightforward there is always the potential for disputes 
                            with beneficiaries and creditors including Her Majesty's Revenue and Customs, 
                            so it is important to realise that if you make a mistake you can face 
                            unlimited personal legal and financial liability. The estate cannot be 
                            fully distributed until Inheritance Tax is paid - so if funds can't be 
                            released from the estate you may need to take out a short-term loan to 
                            pay IHT (Inheritance Tax on property can usually be deferred and payment 
                            by instalments negotiated) and this may also apply if there is no funeral 
                            provision.  If you have agreed to become an executor and then decide against 
                            it, after someone has died, it is important not to undertake any actions 
                            in relation to the will or the estate as you may become legally liable.</li>
                          <li><strong>Make sure you have found the <i>last</i> will and testament</strong><br />if you do agree 
                          to become an executor for someone make sure you know who their solicitor is or 
                          who holds their last will and testament - ask for a copy of the will and 
                          any accompanying letter of intent, so that you have a good understanding 
                          before they die of how they would like their estate distributed.  If you 
                          have not located the correct will and close the estate the consequences 
                          can be very serious. Furthermore, all sorts of problems can arise when a 
                          will is poorly drafted as any ambiguity can make executors more vulnerable 
                          to liability through error. Many people now choose to write their own wills 
                          using a prepared form from a stationery shop - if this is the case, make 
                          sure that it is valid (for example it must be signed by two completely 
                          independent witnesses).</li>
                          <li><strong>Make sure you get several copies of the death certificate and have all 
                          the other information you need to proceed efficiently</strong><br /> 
                          if you are registering 
                          someone's death you need to contact their GP within 5 days of their death 
                          to get a valid medical certificate which will then allow you to register 
                          the death. It is important to get several copies of the death certificate 
                          to ensure that they are available for life insurance, pension companies, 
                          banks, employers etc. Other personal information that you will need are 
                          the deceased person's NI and NHS number, date and place of birth, date 
                          of marriage if applicable, and tax reference number.</li>
                          <li><strong>Find out as soon as you can if you need to apply for a Grant of 
                          Probate</strong><br />applying for probate will give you the legal authority to
                           administer and distribute a deceased person's estate. If the estate
                            is valued at below &pound;5,000 or it is being passed directly to a spouse 
                            grant of probate is not usually necessary - but in most other cases 
                            you will need to apply. Visit your probate registry to get advice 
                            and help, but remember that you can only apply for grant of probate 
                            if someone has written a will and named you as the executor, and you 
                            can only receive it after you have valued the estate and paid Inheritance 
                            Tax. You will then be able to pay other debts and share the remains 
                            of the estate amongst beneficiaries. If you are acting for a close 
                            family relative who hasn't written a will (someone who has died 
                            intestate), you can apply to become the administrator and receive 
                            'letters of administration'; this role carries the same legal 
                            responsibilities as an executor.</li>
                        <li><strong>READ the will carefully</strong><br /> you need to really understand the contents 
                        of the will and the basic legal terminology used to make gifts and create 
                        trusts etc. If you have any doubts or concerns seek professional legal 
                        advice as this can save a lot of problems in the long term. If you are 
                        not happy to proceed you can formally relinquish the role of executor to 
                        a solicitor or bank (you will need to inform the Probate Office and sign 
                        a Deed of Renunciation) and you must do this before you carry out any 
                        'executor' duties.</li>
                        <li><strong>Finding Beneficiaries and Claimants</strong><br /> it is important that you show you 
                        have done everything within your power to track down estate beneficiaries 
                        and claimants. You should notify the general public of the estate, 
                        normally by placing a notice in the London Gazette (the official Government 
                        Journal of Record). If the estate includes land you are also expected to 
                        put a notice in the relevant local newspaper.  If a beneficiary or creditor 
                        appears after the estate has been distributed, the executor can still be 
                        held financially liable.</li>
                        <li><strong>Ensure you have gathered in all assets and considered all liabilities</strong> 
                        <br />
                        probate is the period in which you account for all assets and liabilities 
                        and work out if Inheritance Tax (IHT) is due although you can ask HMRC 
                        to work out the tax liability for you. The first instalment must be paid 
                        within six months of the person's death, starting from the end of the 
                        month in which they died (NB: this usually excludes tax payable on any 
                        property which can be deferred, though interest maybe payable). You cannot 
                        receive Grant of Probate or pay any other debts or beneficiaries until IHT 
                        has been paid so it is important to work this out as quickly as possible. 
                        Once again if you can talk to the person while they are alive it is a good 
                        idea to ask for account details and passwords, details of shares, overseas 
                        assets or property, and any long term debts including credit cards or hire 
                        purchase schemes. </li>
                        <li><strong>Keep on top of the job</strong><br /> it is vital to keep an accurate and up-to-date paperwork 
                        trail in case beneficiaries or creditors ask to see how you are managing the estate; 
                        this also helps you to keep track of your own expenses and maintain detailed probate 
                        accounts for HMRC, which must be submitted to the Revenue even if Inheritance Tax 
                        isn't payable. Your role is to protect the estate for beneficiaries so ensure that 
                        any property is still insured prior to sale and that if it is uninhabited the 
                        insurer knows this. You should also let utility companies know if the main house 
                        is vacant so that the estate is not paying any unnecessary bills.</li>
                        <li><strong>Get a second (or third) opinion when you are valuing property 
                        for probate</strong><br /> it is very important that you have documented proof that property 
                        belonging to the estate has been accurately valued, including the main home and 
                        its contents, as well as valuable objects such as antiques, jewellery or vehicles. 
                        It is advisable that you ask more than one qualified person for current written 
                        valuations.  Executors are ultimately held responsible for valuing an estate 
                        and if HMRC believe you have undervalued property (even inadvertently) they 
                        may pursue you for unpaid tax and impose a penalty for negligence - the penalty 
                        can be between 30-100% of the additional tax liability. In 2010 HMRC challenged 
                        over 9,300 property valuations submitted for probate (60%), 30% of which 
                        were upheld. HMRC recommends using a professional valuer or surveyor in addition 
                        to or instead of a high street estate agent. </li>
                        <li><strong>Protect your own interests</strong><br /> As an executor you can 
                        reclaim 'reasonable expenses' but will not to be paid a salary/fee, unless 
                        expressly stated in the will or unless you are employed as a professional 
                        executor, so make sure you keep an accurate running total of your costs. 
                        It is worth considering executors insurance, which is a form of indemnity 
                        insurance very similar to the type of professional liability insurance 
                        held by a solicitor. Executors insurance can protect you against any 
                        legal or financial claims that result from your actions and the cost 
                        of insurance can often be reclaimed from the estate as part of your expenses.</li>
                    </ul>
                  </div>
               </div>
               <div class="col-md-2">
               </div>
           </div>
       </div>
      <!--END TIER 1-->
      
      
<?php include 'footer.php'; ?>
