<?php include 'header.php'; ?>
    <div class="breadcrumbsWrapper row">
    	<div class="container">
                <div class="row">
                    <div class="col-md-12">
                          <ul class="breadcrumbs clearfix">
                                <li><a href="index.php">Home</a></li>
                                <li class="current">Frequently Asked Questions</li>
                          </ul>
                    </div>
              </div>
        </div>
    </div>
      
      <!--TIER 1-->
      <div class="row">
          <div class="container">
                <div class="row">
                    <div class="doublepad clearfix">
                    	   <div class="col-md-1">
                         </div>
                        <div class="col-md-10">
                            <h1>Frequently Asked Questions</h1>
                            <div class="borderBottom doublepadb stdpadt">
                                <h5>Why do I need insurance protection as an executor of a will or administrator?</h5>
                                <p>Being an <strong>executor</strong>, or  <strong>administrator</strong> is a serious 
                                legal responsibility and any 
                                executor faces the possibility of unlimited personal liability if they make mistakes 
                                in the distribution of an estate. Professional executors are protected by 
                                professional indemnity but lay executors/personal representatives , administrators and 
                                rarely have any form of legal or financial protection. Insurance is not a legal 
                                requirement for an executor or estate administrator, it is a matter of choice but 
                                it can give you the reassurance you need to carry out the executor's duties 
                                effectively without worrying about the repercussions of any financial or legal 
                                claims that might be made against you. <strong>The cost of taking out executors insurance 
                                can usually be recouped as part of the estate administration.</strong></p>
                            </div>
                            <div class="borderBottom doublepadv">
                                <h5>How long will an Executors Insurance policy last for?</h5>
                                <p>Our policy will protect you for one year but you can extend the policy on an 
                                annual basis with a <strong>discount of 10% on the original base premium.</strong></p>
                            </div>
                            <div class="borderBottom doublepadv">
                                <h5>Is a policy period of 12 months long enough to protect me as an executor?</h5>
                                <p>The administration of most estates generally takes less than 12 months - however 
                                the Limitation Act 1980 does stipulate that certain claims can be made <strong>against the 
                                executor for up to 12 years after the death</strong> of the estate owner. In most cases, 
                                problems with the estate tend to arise during the early stages of probate but <strong>we 
                                can extend your Executors Insurance on an annual basis</strong> if you wish to continue 
                                your cover beyond the first year. Any additional extensions will be <strong>discounted 
                                by 10%</strong> on the original base premium.</p>
                            </div>
                            <div class="borderBottom doublepadv">
                                <h5>Do I need to a policy which covers the entire value of the estate?</h5>
                                <p>You can choose your own level of cover. Some people will feel more comfortable 
                                if their insurance covers the full value of the estate while others will choose a 
                                level of cover which they consider protects them against reasonable financial 
                                and legal risk. In reality it is unlikely that many executors will be in a 
                                position where they become liable for the entire value of the estate.</p>
                            </div>
                            <div class="borderBottom doublepadv">
                                <h5>If there is more than one executor for an estate will Executors Insurance 
                               offer protection to all the executors?</h5>
                                <p>Our standard Executors Insurance applies to an individual named executor or 
                                administrator, however we can extend policies to include two additional named 
                                executors when you make your initial application online <strong>(we can not extend the 
                                policy to include additional executors retrospectively)</strong> - additional executors 
                                can take advantage of our discounted rate <strong>(15% off the original premium for 
                                additional executors up to a maximum of two people)</strong>. Where cover is applied 
                                to multiple executors each executor will be added to the main policy. If you 
                                need more than three executors on one policy you will need to contact us 
                                directly at <a href="mailto:enquiries@executorsinsurance.co.uk">
                                enquiries@executorsinsurance.co.uk</a></p>
                            </div>
                            <div class="borderBottom doublepadv">
                                <h5>I already have an Executors Insurance policy with you but another executor 
                               now wants to be added to this policy can you arrange this?</h5>
                                <p>We can arrange an individual policy for them which will give them personal 
                                protection but we cannot extend your policy to include other executors 
                                retrospectively.</p>
                            </div>
                            <div class="doublepadt">
                               <h5>I only found out about Executors Insurance after becoming an executor - 
                               can I back date my cover?</h5>
                                <p>We can provide your with cover but we are only able to accept a retrospective 
                                policy within a three month period of Grant of Probate and provided there 
                                have been no complaints or disputes arising from your role as an executor or 
                                estate administrator. All you need to do is provide us with the exact date on 
                                which you legally accepted your role and we can backdate your policy cover. 
                                Your cover will then run from 6 months from that start date unless you choose 
                                to extend it for another year.</p>
                            </div>
                       </div>
                       <div class="col-md-1">
                       </div>
                   </div>
               </div>
          </div>
      </div>
      <!--END TIER 1-->
      
      
<?php include 'footer.php'; ?>
