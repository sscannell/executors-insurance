<?php include 'header.php'; ?>
    <div class="breadcrumbsWrapper row">
    	<div class="container">
                <div class="row">
                    <div class="col-md-12">
                          <ul class="breadcrumbs clearfix">
                                <li><a href="index.php">Home</a></li>
                                <li class="current">Useful Links</li>
                          </ul>
                    </div>
              </div>
        </div>
    </div>
      
      <!--TIER 1-->
        <div class="row">
            <div class="doublepadv clearfix">
                   <div class="col-md-2">
                        <?php include 'side-nav-executor.php'; ?>
                 </div>
                <div class="col-md-8">
                	<div class="doublepadh">
                    <h1>Further Information for Executors and Estate Administrators</h1>
                    <p>There is a huge amount of information available to executors - a good 
                    starting point is the <a href="http://www.justice.gov.uk/courts/probate"
                    target="_blank">Government Probate Service website.</a></p>
                    <p>To find a detailed explanation of the Trustee Act 2000 visit the Government 
                    Legislation Website <a href="http://www.legislation.gov.uk/ukpga/2000/29/contents" 
                    target="_blank">Trustee Act 2000</a>.</p>
                    <h2>Other Online Information for Executors and Estate Administrators</h2>
                    <p><a href="http://www.death-duties.co.uk" target="_blank">www.death-duties.co.uk</a>
                     offers executors and anyone else dealing with the 
                    affairs of a deceased estate a comprehensive and practical guide.</p>
                    <p><a href="http://www.executorservices.co.uk" target="_blank">www.executorservices.co.uk</a> 
                    provides comprehensive support for executors 
                    dealing with probate property. Probate property can be difficult to handle and 
                    it is likely to be the most valuable asset within an estate. The Executor is 
                    responsible for ensuring that it is managed correctly -it is the duty of an 
                    Executor to secure, insure and maintain the property and there is also a legal 
                    obligation to demonstrate that the best possible outcome has been achieved to 
                    satisfy all beneficiaries. Since 1997 Residential property solutions, provided 
                    by Move with Us, have been used widely within the UK by financial institutions, 
                    new home developers, trusts, charities and solicitors. They now provide a bespoke 
                    property management service for Executors and Estate Administrators.</p>
                    <p>If you are looking for a way to do probate yourself, try <a 
                    href="http://www.thelawwizard.com/" target="_blank">The Probate Wizard</a>. 
                    It is a unique online DIY probate service which is designed to keep the cost 
                    of probate down and make the whole process simple and convenient. All the forms 
                    you need are compiled accurately, giving you all you need to apply for the grant 
                    of probate.</p>
                  </div>
               </div>
               <div class="col-md-2">
               </div>
           </div>
       </div>
      <!--END TIER 1-->
      
      
<?php include 'footer.php'; ?>
