<?php include 'header.php'; ?>
    <div class="breadcrumbsWrapper row">
    	<div class="container">
                <div class="row">
                    <div class="col-md-12">
                          <ul class="breadcrumbs clearfix">
                                <li><a href="index.php">Home</a></li>
                                <li class="current">Executor Details</li>
                          </ul>
                    </div>
              </div>
        </div>
    </div>

      <!--TIER 1-->
        <div class="row">
            <div class="doublepadv clearfix">
                   <div class="col-md-2">
                        <?php include 'side-nav-executor.php'; ?>
                 </div>
                <div class="col-md-8">
                	<div class="doublepadh">
                    <h1>Executors</h1>
                    <h2>Executors and Statutory Duty of Care</h2>
                    <p>The Trustee Act of 2000 which extends to England and Wales, has 
                    clarified the law on the management of deceased estates in relation 
                    to both professional and lay executors.</p>
                    <p><strong>All executors have a statutory duty of care</strong> to carry 
                    out the administration 
                    of an estate with care and skill - which broadly means that the executor 
                    must act in the best interests of the beneficiaries and avoid loss or 
                    injury to the estate.</p>
                    <p>This Act outlines that the status of an executor, their professional 
                    occupation, will be taken into consideration in the event of a complaint 
                    or legal action. The Act has imposed both legal responsibilities and 
                    unlimited personal liability on all executors, (mistakes and poor management 
                    by a professional executor would be viewed more critically than errors 
                    made by a lay executor -this proviso is also partially dependent on the 
                    executor's special knowledge, experience or professional status).</p>
                    <h2>What is the difference between an Executor and Administrator?</h2>
                    <p><strong>Executors</strong> - An executor or group of executors is named in a will and 
                    given rights of management over the deceased's estate. The executor must 
                    apply for 'grant of probate' through the local probate registry to proceed 
                    with the management of the estate.</p>
                    <p><strong>Administrators</strong> - If someone dies intestate (dies without writing a will) 
                    or if a will has been written but cannot be found a close relative will apply 
                    for the right to administrate the deceased estate - an administrator of an 
                    estate is appointed by the Probate Registry. The administrator must apply 
                    to the Probate Registry for 'a grant of letters of administration' of the 
                    estate in order to to proceed with probate.</p>
                 </div>
               </div>
               <div class="col-md-2">
               </div>
           </div>
       </div>
      <!--END TIER 1-->
      
      
<?php include 'footer.php'; ?>
