<?php include 'header.php'; ?>
    <div class="breadcrumbsWrapper row">
    	<div class="container">
                <div class="row">
                    <div class="col-md-12">
                          <ul class="breadcrumbs clearfix">
                                <li><a href="index.php">Home</a></li>
                                <li class="current">Case Studies</li>
                          </ul>
                    </div>
              </div>
        </div>
    </div>
      
      <!--TIER 1-->
      <div class="row">
          <div class="container">
                <div class="row">
                    <div class="doublepad clearfix">
                    	   <div class="col-md-1">
                         </div>
                        <div class="col-md-10">
                            <h1>Case Studies</h1>
                            <h2>Case Against Lay Executors</h2>
                            <p>For most executors the process of probate should be relatively 
                            straightforward, particularly where the estate is small and there is no 
                            inheritance tax to contend with, however there are many recorded instances 
                            where lay <strong>executors and administrators have been subject to serious and 
                            unexpected claims</strong> - sometimes from other beneficiaries or creditors and 
                            increasingly from HMRC in pursuit of additional inheritance tax particularly 
                            in relation to property (the HMRC v Lever case below is a good example of 
                            recent cases -in fact in 2010 HMRC pursued 9,368 cases where they felt 
                            property had been undervalued for probate).</p>
                            <p>We have listed some <strong>real life cases</strong> which have been 
                            <strong>brought against lay 
                            executors</strong> to show the different types of claims that can arise from HMRC, 
                            beneficiaries, problems over the validity of wills including mutual wills.</p>
                            <br />
                            <h4>HMRC vs Lever</h4>
                            <p>In 2007 Mr Lever, acting as executor for his late mother-in-law's estate paid 
                            &pound;400,000 in inheritance tax, closed the accounts, and distributed the balance of 
                            the estate to beneficiaries of her will. Four months later he received a letter 
                            from HMRC saying they were re-considering the value on his late mother-in-law's 
                            home - the house had orginally been valued by a local estate agent at &pound;1.4 million 
                            (this was declared in the IHT200 form and submitted to HMRC) but it eventually 
                            went on to sell through a non-binding sealed-bid auction for over &pound;2 million.</p>
                            <p>Although Mr Lever paid the full value of inheritance tax on the &pound;2 million sold 
                            value of the property and not on the original estate agent's lower estimate HMRC 
                            said they were considering enforcing a personal penalty for negligence on Mr Lever 
                            for the sum of &pound;44,000 because of the information he submitted in his original 
                            IHT200 form. He refused to pay the penalty, arguing that he had not been negligent 
                            but had informed them promptly when the house sold for more than its estimated value 
                            and paid all inheritance tax in full.</p><br />
                            <h4>Thompson vs Fraser</h4>
                            <p>Two sisters made detailed mutual wills (leaving their estates to each other) in 
                            1991. The wills stated that when one sister predeceased the other, further clauses 
                            in each other's wills would come into effect, leaving their estates to 15 beneficiaries. 
                            In spite of this agreement however, in 2003, the surviving sister decided to change 
                            her will to add two new beneficiaries including her hairdresser. Then in 2006 she 
                            changed her will for a second time leaving the entire estate to her hairdresser, 
                            Ms Fraser, whom she also appointed as sole executrix. Upon her death, this had 
                            the effect of leaving Ms Fraser with &pound;300,000.</p>
                            <p>The claimants in this case - who were some of the beneficiaries under the 
                            original mutual wills - brought action claiming the original mutual wills were 
                            still valid, therefore requesting the new executor and beneficiary return the 
                            &pound;300,000. The court found there was sufficient evidence of intention when the 
                            original mutual wills were created for them to still be in force; therefore, 
                            the executor had to reimburse the estate.</p><br />
                            <h4>Shovelar vs Lane</h4>
                            <p>Here, in a further case of mutual wills, two widowers with their own separate 
                            families married each other and created their mirror wills leaving everything 
                            to each other, with the provision that afterwards, the estate would be equally 
                            divided among all their children. However, upon the wife's death, her husband 
                            altered his will, leaving the estate to just his descendants, and naming his 
                            son-in-law as one of the executors.</p>
                            <p>The claimants - members of the wife's family - succeeded in claiming that the 
                            previous wills should be treated as mutual wills and so were in fact 
                            still valid.</p>
                            <p>Initially, the executors were allowed to reimburse their costs out of the 
                            estate, but due to legal bills amounting to over &pound;350,000 for a &pound;134,000 estate 
                            this left the claimants with nothing; this fact, and the extent to which the 
                            executors had become involved in the dispute, led the Court of Appeal to 
                            conclude that the executors should be personally liable for the entirety 
                            of the costs.</p> <br />
                            <h4>IRC vs Stannard</h4>
                            <p>Here, the Inland Revenue attempted to sue the executor of a deceased's estate 
                            for approx. &pound;75,000 capital transfer tax and interest. The defendant claimed 
                            that firstly, as executor he was only a representative of the estate and not 
                            personally liable, and secondly, as a resident of Jersey he was immune from 
                            any English legal action. However, it was held that both the court had 
                            appropriate jurisdiction under which it could bring the case, and furthermore, 
                            the executor could be sued in de bonis propriis form; therefore, he was personally 
                            liable for the amount - which, by the end of the case had reached over &pound;90,000 
                            plus the Revenue's costs. </p>
                       </div>
                       <div class="col-md-1">
                       </div>
                   </div>
               </div>
          </div>
      </div>
      <!--END TIER 1-->
      
      
<?php include 'footer.php'; ?>
