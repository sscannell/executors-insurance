<?php include 'header.php'; ?>
    <div class="breadcrumbsWrapper row">
    	<div class="container">
                <div class="row">
                    <div class="col-md-12">
                          <ul class="breadcrumbs clearfix">
                                <li><a href="index.php">Home</a></li>
                                <li class="current">Blog</li>
                          </ul>
                    </div>
              </div>
        </div>
    </div>

      <!--TIER 1-->
        <div class="row">
            <div class="doublepadv clearfix">
                   <div class="col-md-2">
                        <?php include 'side-nav-blog.php'; ?>
                 </div>
                <div class="col-md-8">
                	<div class="doublepadh">
                    <h1>Blog</h1>
                    <div class="media borderBottom doublepadv">
                      <a class="pull-left" href="#">
                        <img class="media-object img-circle"
                        height="150px" width="150px" src="images/default.jpg" />
                      </a>
                      <div class="media-body">
                        <span class="numberCount">
                        9th Dec
                        </span>
                        <h2 class="media-heading">
                        The Guardian looks at What is Involved in Probate</h2>                        
                        <p>The Guardian's Money Editor, Patrick Collins, reports on people 
                        taking on the role of executor because of prohibitive bank and 
                        solicitors' costs but are there sufficient warnings about 
                        the potential liabilities that lay executors are exposed to?</p>
                            <a href="#" class="btn btn-lg btn-red pull-right">Read More</a>
                      </div>
                    </div>
                    <div class="media borderBottom doublepadv">
                      <a class="pull-left" href="#">
                        <img class="media-object img-circle"
                        height="150px" width="150px" src="images/default.jpg" />
                      </a>
                      <div class="media-body">
                        <span class="numberCount">
                        6th Dec
                        </span>
                        <h2 class="media-heading">Government Donation Puts Executors in Firing Line</h2>
                        <p>The furore which has erupted over Joan Edward's &pound;520,000 donation 
                        to the government may well put her executors in the firing line. Her 
                        legacy was shared between the Conservative and Liberal parties, 
                        as the acting coalition government of the day, but the Labour</p>
                            <a href="#" class="btn btn-lg btn-red pull-right">Read More</a>
                      </div>
                    </div>
                    <div class="media borderBottom doublepadv">
                      <a class="pull-left" href="#">
                        <img class="media-object img-circle"
                        height="150px" width="150px" src="images/default.jpg" />
                      </a>
                      <div class="media-body">
                        <span class="numberCount">
                        6th Dec
                        </span>
                        <h2 class="media-heading">Government Donation Puts Executors in Firing Line</h2>
                        <p>The furore which has erupted over Joan Edward's &pound;520,000 donation 
                        to the government may well put her executors in the firing line. Her 
                        legacy was shared between the Conservative and Liberal parties, 
                        as the acting coalition government of the day, but the Labour</p>
                            <a href="#" class="btn btn-lg btn-red pull-right">Read More</a>
                      </div>
                    </div>
                 </div>
               </div>
               <div class="col-md-2">
               </div>
           </div>
       </div>
      <!--END TIER 1-->
      
      
<?php include 'footer.php'; ?>
